import puppeteer  from 'puppeteer'
import yargs from 'yargs'
import debug from 'debug'

const argv = yargs.argv
const log = debug('@anue-parser:engine')

const MAX_WORKER = argv.worker || 2
let pageInstanceCount = 0
let jobQueue:Array<(args:any) => void> = []
let count = 0

export default class EngineProvider {

  private static browser:puppeteer.Browser | null = null

  /**
   * Method that get the singleton Browser instance
   */
  private static getBrowser = async ():Promise<puppeteer.Browser> => {

    if(!EngineProvider.browser) {
      EngineProvider.browser = await puppeteer.launch({ 
        headless : argv.headless !== 'false' 
      })
      log('new browser created %o', ++count)
    }
    return EngineProvider.browser
  }

  /**
   * Returns a promise which resolves a Page instance, it holds until
   * page instance count is lower than MAX_WORKER count.
   */
  static getAvailableContext = async ():Promise<any> => {

    
    if(++pageInstanceCount <= MAX_WORKER) {
      const browser = await EngineProvider.getBrowser()
      log('create page => pool = %s max = %s', pageInstanceCount, MAX_WORKER)
      return await browser.newPage()
    }
    else 
      --pageInstanceCount

    // instance count reached limit, put the promise in jobQueue
    const p = new Promise((resolve) => {
      jobQueue.push(resolve)
    })

    return await p
  }

  /**
   * Release a page instance from the pool.
   */
  static releaseContext = async () => {
    
    // if the browser instance does not exists which means there's no page context 
    // to release
    if(!EngineProvider.browser)
      return
    log('release context, remained jobs in queue => %s, pool => %s', jobQueue.length, pageInstanceCount)
    if(--pageInstanceCount <= MAX_WORKER) {
      if(jobQueue.length <= 0)
        return
      const resolver = jobQueue.shift()
      const page = await EngineProvider.browser.newPage()
      pageInstanceCount++
      resolver && resolver(page)
    }
    else 
      ++pageInstanceCount

  }
}