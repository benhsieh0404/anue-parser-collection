/**
 * Generate unified formatted logger for specific module
 */

import debug from 'debug'

export default class LoggerProvider {
  
  static getLogger = (moduleName:string) => {
    return {
      log: debug(`@anue-parser:${moduleName}:log`),
      info: debug(`@anue-parser:${moduleName}:info`),
      error: debug(`@anue-parser:${moduleName}:error`),
    }
  }

}