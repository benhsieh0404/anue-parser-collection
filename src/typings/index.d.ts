declare enum ParserFormatOptions {
  JSON = 'json',
  CSV = 'csv'
}

interface IParserResult {

  bankCode:number,
  currencyCode:string,
  rates: Array<{ name:string, value:number }>

}

interface Element { }
interface Node { }
interface NodeListOf<TNode = Node> { }

interface ParserOptions {
  // name of parser
  use:string
  // format of task result 
  format:ParserFormatOptions
  // boolean value that specifies if the task output goes to stdout
  stdout:boolean
}
