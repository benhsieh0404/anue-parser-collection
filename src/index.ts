import { BaseParser } from './parsers/base'
import parsers from './parsers'
import yargs from 'yargs'
import _ from 'lodash'
import Logger from './lib/log'
import web from './web'

const argv = yargs.argv
const { log } = Logger.getLogger('sys')

if(argv.daemon || argv.port) {
  web()
}

const run = async (args:any) => {
  try {
    log('initiate browser instance');
    const parserId = args.use
    const resolver = _.get(parsers, parserId)
    if(!resolver) {
      throw Error(`Parser [${parserId}] not defined`)
    }
    const instance:BaseParser = await new resolver();
    log('executing parser [%s] with params %o', instance.getName(), args);
    log('finished');
    await instance.resolve(args);
  }
  catch(err) {
    console.log(err.stack)
  }
  finally {
    !argv.daemon && process.exit(0)
  }
}

// process run by TTY, for example :
// node src --use=feib --format=json 
if (process.stdin.isTTY) {
  argv.use && run(argv)
}
// accept piping content
// cat batch.json | node src/index.js
else {
  let data = '';
  process.stdin.setEncoding('utf8');
 
  process.stdin.on('readable', function() {
    let chunk;
    while (chunk = process.stdin.read()) {
      data += chunk;
    }
  })
 
  process.stdin.on('end', function () {
    data = data.replace(/\n$/, '');
    // doing batch job in sequence
    const task = JSON.parse(data)
    log('execute jobs from JSON', task);
    for(let t of task.jobs) {
      run(t)
    }
  })
}

