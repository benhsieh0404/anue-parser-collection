import express from 'express'
import yargs from 'yargs'
import Logger from '../lib/log'
import Parsers from '../parsers'
import _ from 'lodash'

const { log } = Logger.getLogger('parser-web')
const argv = yargs.argv

export default async function() {

  const web = express()

  // middleware for logging request information
  web.use((req, res, next) => {
    log('[%s] request %s %o ',req.method, req.path, req.query)
    next()
  })
  
  web.get('/:parser', async(req, res) => {
    
    try {

      const parserId = req.params.parser
      const parserClass = _.get(Parsers, [parserId])
      log('user parser [%s] options = %o', parserId, req.query)
      if(!parserClass) {
        res.status(400).send({
          error: `Specified parser "${parserId}" does not exists`,
          data: null,
        })
      }
      else {
        const data = await new parserClass().resolve(req.query)
        res.send({
          error: null,
          data,
        })
      }
    }
    catch(error) {
      res.status(500).send({ error })
      log(error.stack)
    }
    res.end()

  })

  web.listen(argv.port || 3000, (err:any) => {
    if(!err)
      log('application listen on port %s', argv.port);
    else 
      throw err;
  })

}

