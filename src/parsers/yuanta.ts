/**
 * @description FEIB parser
 * The script crawling data from the following page:
 * https://ebank.feib.com.tw/netbank/servlet/TrxDispatcher?trx=com.lb.wibc.trx.Accessible.AccessibleExchangerate&state=AccessibleExchangerate2&savingType=2
 */

import puppeteer from 'puppeteer'
import { BaseParser } from './base'
import EngineProvider from '../lib/engine'
import _ from 'lodash'
import yargs from 'yargs'
import Logger from '../lib/log'

const  { log, error } = Logger.getLogger('yuanta-parser')
const argv = yargs.argv

interface YuantaParserOptions {
  currencyType: string,
}

interface YuntanParserCache {
  // time stamp the cache last updated
  last:number
  rates: {
    [key:string]: IParserResult
  }
}

let cache:YuntanParserCache = {
  last: -1,
  rates: {}
}

export default class YuantaParser extends BaseParser {

  getName() {
    return '元大銀行'
  }

  async resolve (options:YuantaParserOptions):Promise<IParserResult>  {
    
    let page:any

    try {

      const url = 'https://www.yuantabank.com.tw/bank/exchangeRate/foreignCurrency.do'
      const currencyCode:string = options.currencyType
      let cachedResult = cache.rates[currencyCode]

      // Return cached result when cache has been updated in 2 minutes
      if(Date.now() - cache.last < 2 * 60000 && cachedResult) {
        log('resolving rate interest data from cache %o ', cachedResult)
        return cachedResult
      }

      page = await EngineProvider.getAvailableContext()
      
      await page.goto(url)
      
      log('wait for page to complete ..')
      await page.waitFor('tbody')

      log(`parse %s `, url)
      
      const data = await this.toJSON(page)
      
      cache.last = Date.now()
      cache.rates = data

      log('Update cache => %o', data)
      log('resolving rate interest data %o ', data[currencyCode])

      return data[currencyCode]
      
    }
    catch(err) {
      error('error %s', err)
      throw err
    }
    finally {
      page && page.close && page.close()
      EngineProvider.releaseContext()
    }

  }

  async toJSON(page:puppeteer.Page) {
    return await page.evaluate(`
        const rows = Array.from(document.querySelector('.rate_interest').querySelectorAll('tr'))
        // row 0 is useless
        rows.shift()
        // row 1 contains headers
        const headers = rows.shift()
        const fields = []
        for(const h of headers.children) {
          fields.push(String(h.innerText).replace('\t', ''))
        }
        let rates = {}
        for(const r of rows) {
          let code = ''
          for(let i=0; i< fields.length; i++) {
            if(i === 0) {
              code = /[A-Z]+/.exec(r.children[i].innerText)[0]
              rates[code] = {
                bankCode: 806,
                currencyCode: code,
                rates: []
              }
            }
            let node = r.children[i+1]
            rates[code].rates.push({
              name: fields[i],
              value: node ? parseFloat(node.innerText) : 0
            })
          }
        }
        rates
      `)
  }
  
}
