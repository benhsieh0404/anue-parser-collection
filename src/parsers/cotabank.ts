import puppeteer from 'puppeteer'
import EngineProvider from '../lib/engine'
import { BaseParser } from '../parsers/base'
import _ from 'lodash'
import yargs from 'yargs'
import Logger from '../lib/log'

const  { log, error } = Logger.getLogger('feib-parser')
const argv = yargs.argv
// https://ebank.cotabank.com.tw/eBank/js/NewRate_2.js

enum COTACurrencyTypeEnum {
  USD = "USD",
  GBP = 'GBP',
  CHF = 'CHF',
  HKD = 'HKD',
  AUD = 'AUD',
  SGD = 'SGD',
  CAD = 'CAD',
  CNY = 'CNY',
  ZAR = 'ZAR',
  EUR = 'EUR',
  NZD = 'NZD',
}

interface COTAParserOptions extends ParserOptions {
  currencyType:COTACurrencyTypeEnum
}

export default class COTABankParser extends BaseParser {

  getName(): string {
    return '三信商業銀行';
  }

  async resolve(options: COTAParserOptions): Promise<IParserResult> {
    let page:any

    try {

      const url = 'https://www.cotabank.com.tw/web/interest_2/'
      page = await EngineProvider.getAvailableContext()

      await page.goto(url)

      log('wait for page to complete ..')
      await page.waitFor('#T1')

      log(`parse %s `,url)

      // get available currency types from drop-down menu element
      let currencyTypes = await page.evaluate(`
        let result = []
        document.querySelectorAll('#T1 tr:not(.hide) td[data-title="幣別："]').forEach(d => {
          result.push(d.innerText)
        })
        result
      `)

			const retrieveCurrencyCode = currencyTypes.map((currency:any) => currency.match(/[A-Z]+/)[0]);

			log('supported currency types .. %o', retrieveCurrencyCode)

			const index = retrieveCurrencyCode.indexOf(options.currencyType)

      if(index < 0) {
        throw `Failed to match currency type ${options.currencyType}`
      }

      await page.waitFor('#T1')
      const output = options.format === 'csv'
        ? await this.toCSV(page)
        : await this.toJSON(page)
			argv.stdout === 'true' && console.log(output)

      return {
        bankCode: 147,
        currencyCode: options.currencyType,
        rates: output[options.currencyType]
      }

    }
    catch(err) {
      error('error %s', err)
      throw err
    }
    finally {
      page && page.close && page.close()
      EngineProvider.releaseContext()
    }
  }

  async toCSV(page:puppeteer.Page) {
    return await page.evaluate(`
      let csv = ''
      // parse table data
      const rows = document.querySelectorAll('tr')
      for(let i=0; i < rows.length; i++) {
        for(let j=0; j < rows[i].childElementCount; j++) {
          csv += rows[i].children[j].textContent + ','
        }
        csv += '\n'
      }
      // make console return value of "csv"
      csv
    `)
  }

  async toJSON(page:puppeteer.Page) {
    return await page.evaluate(`
		// parse table data
		const rows = document.querySelectorAll('#T1 tr:not(.hide) td')
		let data = {}
		let currency;
		for(let i=1; i < rows.length; i++) {
			let getNewCurrency = rows[i].dataset.title === '幣別：' && rows[i].innerText;
			currency = getNewCurrency || currency;

				if (getNewCurrency) {
					currency = currency.match(/[A-Z]+/)[0];
					data[currency] = [];
				} else {
					const extractNumberString = rows[i].innerText.match(/[0-9\.]+/g);

					data[currency].push({
					name: rows[i].dataset.title,
					value: extractNumberString ? Number(extractNumberString[0]) : null,
				});
			}
		}
		data
    `)
  }

}
