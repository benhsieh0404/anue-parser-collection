/**
 * @description FEIB parser
 * The script crawling data from the following page:
 * https://ebank.feib.com.tw/netbank/servlet/TrxDispatcher?trx=com.lb.wibc.trx.Accessible.AccessibleExchangerate&state=AccessibleExchangerate2&savingType=2
 */

import puppeteer from 'puppeteer'
import { BaseParser } from './base'
import EngineProvider from '../lib/engine'
import _ from 'lodash'
import yargs from 'yargs'
import Logger from '../lib/log'

const  { log, error } = Logger.getLogger('feib-parser')
const argv = yargs.argv

const FEIBCurrencyNameMap = {
  USD: '美金',
  GBP: '英鎊',
  CHF: '瑞士法朗',
  HKD: '港幣',
  AUD: '澳幣',
  SGD: '新加坡幣',
  JPY: '日圓',
  CAD: '加拿大幣',
  CNY: '人民幣',
  ZAR: '南非幣',
  EUR: '歐元',
  NZD: '紐西蘭幣'
}

enum FEIBCurrencyTypeEnum {
  USD = "USD",
  GBP = 'GBP',
  CHF = 'CHF',
  HKD = 'HKD',
  AUD = 'AUD',
  SGD = 'SGD',
  JPY = 'JPY',
  CAD = 'CAD',
  CNY = 'CNY',
  ZAR = 'ZAR',
  EUR = 'EUR',
  NZD = 'NZD',
}

interface FEIBParserOptions extends ParserOptions {
  currencyType:FEIBCurrencyTypeEnum
}

export default class FEIBParser extends BaseParser {

  getName() {
    return '遠東商銀'
  }

  async resolve (options:FEIBParserOptions):Promise<IParserResult>  {

    let page:any

    try {

      const url = 'https://ebank.feib.com.tw/netbank/servlet/TrxDispatcher?trx=com.lb.wibc.trx.Accessible.AccessibleExchangerate&state=AccessibleExchangerate2&savingType=2'
      page = await EngineProvider.getAvailableContext()

      await page.goto(url)

      log('wait for page to complete ..')
      await page.waitFor('#currency > option:nth-child(2)')

      log(`parse %s `,url)

      // get available currency types from drop-down menu element
      let currencyTypes = await page.evaluate(`
        let result = []
        document.querySelectorAll('.currency_item select > option').forEach(d => {
          result.push(d.innerText)
        })
        result
      `)
      log('supported currency types .. %o', currencyTypes)
      const index = currencyTypes.indexOf(FEIBCurrencyNameMap[options.currencyType])

      if(index < 0) {
        throw `Failed to match currency type ${options.currencyType}`
      }

      // programmatically set selected index of drop-down menu
      log('found options at %o .. ', index)
      await page.evaluate(`
        document.querySelector('#currency').selectedIndex = ${index}
      `)
      // click the "submit" button
      await page.click('.currency_item > a > button')
      await page.waitFor('table > tbody > tr:nth-child(2)')
      const output = options.format === 'csv'
        ? await this.toCSV(page)
        : await this.toJSON(page)
      argv.stdout === 'true' && console.log(output)
      return {
        bankCode: 805,
        currencyCode: options.currencyType,
        rates: output
      }

    }
    catch(err) {
      error('error %s', err)
      throw err
    }
    finally {
      page && page.close && page.close()
      EngineProvider.releaseContext()
    }

  }

  async toCSV(page:puppeteer.Page) {
    return await page.evaluate(`
      let csv = ''
      // parse table data
      const rows = document.querySelectorAll('tr')
      for(let i=0; i < rows.length; i++) {
        for(let j=0; j < rows[i].childElementCount; j++) {
          csv += rows[i].children[j].textContent + ','
        }
        csv += '\n'
      }
      // make console return value of "csv"
      csv
    `)
  }

  async toJSON(page:puppeteer.Page) {
    return await page.evaluate(`
      // parse table fields
      let fields = ["name", "value"]
      
      // parse table data
      const rows = document.querySelectorAll('tr')
      let data = []
      for(let i=1; i < rows.length; i++) {
        let d = {}
        for(let j=0; j < rows[i].childElementCount; j++) {
          const fieldName = fields[j]
          const value = rows[i].children[j].textContent
          d[fields[j]] = fieldName === 'value'
            ? parseFloat(value)
            : value
        }
        data.push(d)
      }
      // return value
      data
    `)
  }

}
