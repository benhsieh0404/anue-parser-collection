import puppeteer from 'puppeteer'

export interface BaseParser {

  toCSV(page:puppeteer.Page):Promise<string>
  
}
export abstract class BaseParser {

  abstract getName():string;

  async abstract resolve(options:any):Promise<IParserResult>

  async abstract toJSON(page:puppeteer.Page):Promise<string | object>


}