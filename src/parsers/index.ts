import feib from './feib'
import cotabank from './cotabank';
import yuanta from './yuanta'
import { BaseParser as base } from './base'

export {
  feib,
  base
}

export default {
	147: cotabank,
  805: feib,
  806: yuanta
}
