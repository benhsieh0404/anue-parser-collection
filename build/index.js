"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var parsers_1 = __importDefault(require("./parsers"));
var yargs_1 = __importDefault(require("yargs"));
var lodash_1 = __importDefault(require("lodash"));
var log_1 = __importDefault(require("./lib/log"));
var web_1 = __importDefault(require("./web"));
var argv = yargs_1.default.argv;
var log = log_1.default.getLogger('sys').log;
if (argv.daemon || argv.port) {
    web_1.default();
}
var run = function (args) { return __awaiter(_this, void 0, void 0, function () {
    var parserId, resolver, instance, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, 4, 5]);
                log('initiate browser instance');
                parserId = args.use;
                resolver = lodash_1.default.get(parsers_1.default, parserId);
                if (!resolver) {
                    throw Error("Parser [" + parserId + "] not defined");
                }
                return [4 /*yield*/, new resolver()];
            case 1:
                instance = _a.sent();
                log('executing parser [%s] with params %o', instance.getName(), args);
                log('finished');
                return [4 /*yield*/, instance.resolve(args)];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                err_1 = _a.sent();
                console.log(err_1.stack);
                return [3 /*break*/, 5];
            case 4:
                !argv.daemon && process.exit(0);
                return [7 /*endfinally*/];
            case 5: return [2 /*return*/];
        }
    });
}); };
// process run by TTY, for example :
// node src --use=feib --format=json 
if (process.stdin.isTTY) {
    argv.use && run(argv);
}
// accept piping content
// cat batch.json | node src/index.js
else {
    var data_1 = '';
    process.stdin.setEncoding('utf8');
    process.stdin.on('readable', function () {
        var chunk;
        while (chunk = process.stdin.read()) {
            data_1 += chunk;
        }
    });
    process.stdin.on('end', function () {
        data_1 = data_1.replace(/\n$/, '');
        // doing batch job in sequence
        var task = JSON.parse(data_1);
        log('execute jobs from JSON', task);
        for (var _i = 0, _a = task.jobs; _i < _a.length; _i++) {
            var t = _a[_i];
            run(t);
        }
    });
}
//# sourceMappingURL=index.js.map