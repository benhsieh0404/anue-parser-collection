"use strict";
/**
 * Generate unified formatted logger for specific module
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var debug_1 = __importDefault(require("debug"));
var LoggerProvider = /** @class */ (function () {
    function LoggerProvider() {
    }
    LoggerProvider.getLogger = function (moduleName) {
        return {
            log: debug_1.default("@anue-parser:" + moduleName + ":log"),
            info: debug_1.default("@anue-parser:" + moduleName + ":info"),
            error: debug_1.default("@anue-parser:" + moduleName + ":error"),
        };
    };
    return LoggerProvider;
}());
exports.default = LoggerProvider;
//# sourceMappingURL=log.js.map