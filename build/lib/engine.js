"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var puppeteer_1 = __importDefault(require("puppeteer"));
var yargs_1 = __importDefault(require("yargs"));
var debug_1 = __importDefault(require("debug"));
var argv = yargs_1.default.argv;
var log = debug_1.default('@anue-parser:engine');
var MAX_WORKER = argv.worker || 2;
var pageInstanceCount = 0;
var jobQueue = [];
var count = 0;
var EngineProvider = /** @class */ (function () {
    function EngineProvider() {
    }
    EngineProvider.browser = null;
    /**
     * Method that get the singleton Browser instance
     */
    EngineProvider.getBrowser = function () { return __awaiter(_this, void 0, void 0, function () {
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!!EngineProvider.browser) return [3 /*break*/, 2];
                    _a = EngineProvider;
                    return [4 /*yield*/, puppeteer_1.default.launch({
                            headless: argv.headless !== 'false'
                        })];
                case 1:
                    _a.browser = _b.sent();
                    log('new browser created %o', ++count);
                    _b.label = 2;
                case 2: return [2 /*return*/, EngineProvider.browser];
            }
        });
    }); };
    /**
     * Returns a promise which resolves a Page instance, it holds until
     * page instance count is lower than MAX_WORKER count.
     */
    EngineProvider.getAvailableContext = function () { return __awaiter(_this, void 0, void 0, function () {
        var browser, p;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(++pageInstanceCount <= MAX_WORKER)) return [3 /*break*/, 3];
                    return [4 /*yield*/, EngineProvider.getBrowser()];
                case 1:
                    browser = _a.sent();
                    log('create page => pool = %s max = %s', pageInstanceCount, MAX_WORKER);
                    return [4 /*yield*/, browser.newPage()];
                case 2: return [2 /*return*/, _a.sent()];
                case 3:
                    --pageInstanceCount;
                    _a.label = 4;
                case 4:
                    p = new Promise(function (resolve) {
                        jobQueue.push(resolve);
                    });
                    return [4 /*yield*/, p];
                case 5: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
    /**
     * Release a page instance from the pool.
     */
    EngineProvider.releaseContext = function () { return __awaiter(_this, void 0, void 0, function () {
        var resolver, page;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    // if the browser instance does not exists which means there's no page context 
                    // to release
                    if (!EngineProvider.browser)
                        return [2 /*return*/];
                    log('release context, remained jobs in queue => %s, pool => %s', jobQueue.length, pageInstanceCount);
                    if (!(--pageInstanceCount <= MAX_WORKER)) return [3 /*break*/, 2];
                    if (jobQueue.length <= 0)
                        return [2 /*return*/];
                    resolver = jobQueue.shift();
                    return [4 /*yield*/, EngineProvider.browser.newPage()];
                case 1:
                    page = _a.sent();
                    pageInstanceCount++;
                    resolver && resolver(page);
                    return [3 /*break*/, 3];
                case 2:
                    ++pageInstanceCount;
                    _a.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    }); };
    return EngineProvider;
}());
exports.default = EngineProvider;
//# sourceMappingURL=engine.js.map