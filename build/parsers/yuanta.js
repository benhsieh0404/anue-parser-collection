"use strict";
/**
 * @description FEIB parser
 * The script crawling data from the following page:
 * https://ebank.feib.com.tw/netbank/servlet/TrxDispatcher?trx=com.lb.wibc.trx.Accessible.AccessibleExchangerate&state=AccessibleExchangerate2&savingType=2
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var base_1 = require("./base");
var engine_1 = __importDefault(require("../lib/engine"));
var yargs_1 = __importDefault(require("yargs"));
var log_1 = __importDefault(require("../lib/log"));
var _a = log_1.default.getLogger('yuanta-parser'), log = _a.log, error = _a.error;
var argv = yargs_1.default.argv;
var cache = {
    last: -1,
    rates: {}
};
var YuantaParser = /** @class */ (function (_super) {
    __extends(YuantaParser, _super);
    function YuantaParser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    YuantaParser.prototype.getName = function () {
        return '元大銀行';
    };
    YuantaParser.prototype.resolve = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var page, url, currencyCode, cachedResult, data, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, 6, 7]);
                        url = 'https://www.yuantabank.com.tw/bank/exchangeRate/foreignCurrency.do';
                        currencyCode = options.currencyType;
                        cachedResult = cache.rates[currencyCode];
                        // Return cached result when cache has been updated in 2 minutes
                        if (Date.now() - cache.last < 2 * 60000 && cachedResult) {
                            log('resolving rate interest data from cache %o ', cachedResult);
                            return [2 /*return*/, cachedResult];
                        }
                        return [4 /*yield*/, engine_1.default.getAvailableContext()];
                    case 1:
                        page = _a.sent();
                        return [4 /*yield*/, page.goto(url)];
                    case 2:
                        _a.sent();
                        log('wait for page to complete ..');
                        return [4 /*yield*/, page.waitFor('tbody')];
                    case 3:
                        _a.sent();
                        log("parse %s ", url);
                        return [4 /*yield*/, this.toJSON(page)];
                    case 4:
                        data = _a.sent();
                        cache.last = Date.now();
                        cache.rates = data;
                        log('Update cache => %o', data);
                        log('resolving rate interest data %o ', data[currencyCode]);
                        return [2 /*return*/, data[currencyCode]];
                    case 5:
                        err_1 = _a.sent();
                        error('error %s', err_1);
                        throw err_1;
                    case 6:
                        page && page.close && page.close();
                        engine_1.default.releaseContext();
                        return [7 /*endfinally*/];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    YuantaParser.prototype.toJSON = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, page.evaluate("\n        const rows = Array.from(document.querySelector('.rate_interest').querySelectorAll('tr'))\n        // row 0 is useless\n        rows.shift()\n        // row 1 contains headers\n        const headers = rows.shift()\n        const fields = []\n        for(const h of headers.children) {\n          fields.push(String(h.innerText).replace('\t', ''))\n        }\n        let rates = {}\n        for(const r of rows) {\n          let code = ''\n          for(let i=0; i< fields.length; i++) {\n            if(i === 0) {\n              code = /[A-Z]+/.exec(r.children[i].innerText)[0]\n              rates[code] = {\n                bankCode: 806,\n                currencyCode: code,\n                rates: []\n              }\n            }\n            let node = r.children[i+1]\n            rates[code].rates.push({\n              name: fields[i],\n              value: node ? parseFloat(node.innerText) : 0\n            })\n          }\n        }\n        rates\n      ")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return YuantaParser;
}(base_1.BaseParser));
exports.default = YuantaParser;
//# sourceMappingURL=yuanta.js.map