"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var base_1 = require("./base");
var log = require('../lib/log')('postbank-parser').log;
var PostBankParser = /** @class */ (function (_super) {
    __extends(PostBankParser, _super);
    function PostBankParser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PostBankParser.prototype.getName = function () {
        throw new Error("Method not implemented.");
    };
    PostBankParser.prototype.resolve = function (options) {
        throw new Error("Method not implemented.");
    };
    PostBankParser.prototype.toJSON = function (page) {
        throw new Error("Method not implemented.");
    };
    return PostBankParser;
}(base_1.BaseParser));
exports.default = PostBankParser;
//# sourceMappingURL=postbank.js.map