"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var engine_1 = __importDefault(require("../lib/engine"));
var base_1 = require("../parsers/base");
var yargs_1 = __importDefault(require("yargs"));
var log_1 = __importDefault(require("../lib/log"));
var _a = log_1.default.getLogger('feib-parser'), log = _a.log, error = _a.error;
var argv = yargs_1.default.argv;
// https://ebank.cotabank.com.tw/eBank/js/NewRate_2.js
var COTACurrencyTypeEnum;
(function (COTACurrencyTypeEnum) {
    COTACurrencyTypeEnum["USD"] = "USD";
    COTACurrencyTypeEnum["GBP"] = "GBP";
    COTACurrencyTypeEnum["CHF"] = "CHF";
    COTACurrencyTypeEnum["HKD"] = "HKD";
    COTACurrencyTypeEnum["AUD"] = "AUD";
    COTACurrencyTypeEnum["SGD"] = "SGD";
    COTACurrencyTypeEnum["CAD"] = "CAD";
    COTACurrencyTypeEnum["CNY"] = "CNY";
    COTACurrencyTypeEnum["ZAR"] = "ZAR";
    COTACurrencyTypeEnum["EUR"] = "EUR";
    COTACurrencyTypeEnum["NZD"] = "NZD";
})(COTACurrencyTypeEnum || (COTACurrencyTypeEnum = {}));
var COTABankParser = /** @class */ (function (_super) {
    __extends(COTABankParser, _super);
    function COTABankParser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    COTABankParser.prototype.getName = function () {
        return '三信商業銀行';
    };
    COTABankParser.prototype.resolve = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var page, url, currencyTypes, retrieveCurrencyCode, index, output, _a, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 10, 11, 12]);
                        url = 'https://www.cotabank.com.tw/web/interest_2/';
                        return [4 /*yield*/, engine_1.default.getAvailableContext()];
                    case 1:
                        page = _b.sent();
                        return [4 /*yield*/, page.goto(url)];
                    case 2:
                        _b.sent();
                        log('wait for page to complete ..');
                        return [4 /*yield*/, page.waitFor('#T1')];
                    case 3:
                        _b.sent();
                        log("parse %s ", url);
                        return [4 /*yield*/, page.evaluate("\n        let result = []\n        document.querySelectorAll('#T1 tr:not(.hide) td[data-title=\"\u5E63\u5225\uFF1A\"]').forEach(d => {\n          result.push(d.innerText)\n        })\n        result\n      ")];
                    case 4:
                        currencyTypes = _b.sent();
                        retrieveCurrencyCode = currencyTypes.map(function (currency) { return currency.match(/[A-Z]+/)[0]; });
                        log('supported currency types .. %o', retrieveCurrencyCode);
                        index = retrieveCurrencyCode.indexOf(options.currencyType);
                        if (index < 0) {
                            throw "Failed to match currency type " + options.currencyType;
                        }
                        return [4 /*yield*/, page.waitFor('#T1')];
                    case 5:
                        _b.sent();
                        if (!(options.format === 'csv')) return [3 /*break*/, 7];
                        return [4 /*yield*/, this.toCSV(page)];
                    case 6:
                        _a = _b.sent();
                        return [3 /*break*/, 9];
                    case 7: return [4 /*yield*/, this.toJSON(page)];
                    case 8:
                        _a = _b.sent();
                        _b.label = 9;
                    case 9:
                        output = _a;
                        argv.stdout === 'true' && console.log(output);
                        return [2 /*return*/, {
                                bankCode: 147,
                                currencyCode: options.currencyType,
                                rates: output[options.currencyType]
                            }];
                    case 10:
                        err_1 = _b.sent();
                        error('error %s', err_1);
                        throw err_1;
                    case 11:
                        page && page.close && page.close();
                        engine_1.default.releaseContext();
                        return [7 /*endfinally*/];
                    case 12: return [2 /*return*/];
                }
            });
        });
    };
    COTABankParser.prototype.toCSV = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, page.evaluate("\n      let csv = ''\n      // parse table data\n      const rows = document.querySelectorAll('tr')\n      for(let i=0; i < rows.length; i++) {\n        for(let j=0; j < rows[i].childElementCount; j++) {\n          csv += rows[i].children[j].textContent + ','\n        }\n        csv += '\n'\n      }\n      // make console return value of \"csv\"\n      csv\n    ")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    COTABankParser.prototype.toJSON = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, page.evaluate("\n\t\t// parse table data\n\t\tconst rows = document.querySelectorAll('#T1 tr:not(.hide) td')\n\t\tlet data = {}\n\t\tlet currency;\n\t\tfor(let i=1; i < rows.length; i++) {\n\t\t\tlet getNewCurrency = rows[i].dataset.title === '\u5E63\u5225\uFF1A' && rows[i].innerText;\n\t\t\tcurrency = getNewCurrency || currency;\n\n\t\t\t\tif (getNewCurrency) {\n\t\t\t\t\tcurrency = currency.match(/[A-Z]+/)[0];\n\t\t\t\t\tdata[currency] = [];\n\t\t\t\t} else {\n\t\t\t\t\tconst extractNumberString = rows[i].innerText.match(/[0-9.]+/g);\n\n\t\t\t\t\tdata[currency].push({\n\t\t\t\t\tname: rows[i].dataset.title,\n\t\t\t\t\tvalue: extractNumberString ? Number(extractNumberString[0]) : null,\n\t\t\t\t});\n\t\t\t}\n\t\t}\n\t\tdata\n    ")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return COTABankParser;
}(base_1.BaseParser));
exports.default = COTABankParser;
//# sourceMappingURL=cotabank.js.map