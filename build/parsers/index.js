"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var feib_1 = __importDefault(require("./feib"));
exports.feib = feib_1.default;
var cotabank_1 = __importDefault(require("./cotabank"));
var yuanta_1 = __importDefault(require("./yuanta"));
var base_1 = require("./base");
exports.base = base_1.BaseParser;
exports.default = {
    147: cotabank_1.default,
    805: feib_1.default,
    806: yuanta_1.default
};
//# sourceMappingURL=index.js.map