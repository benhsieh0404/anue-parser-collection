"use strict";
/**
 * @description FEIB parser
 * The script crawling data from the following page:
 * https://ebank.feib.com.tw/netbank/servlet/TrxDispatcher?trx=com.lb.wibc.trx.Accessible.AccessibleExchangerate&state=AccessibleExchangerate2&savingType=2
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var base_1 = require("./base");
var engine_1 = __importDefault(require("../lib/engine"));
var yargs_1 = __importDefault(require("yargs"));
var log_1 = __importDefault(require("../lib/log"));
var _a = log_1.default.getLogger('feib-parser'), log = _a.log, error = _a.error;
var argv = yargs_1.default.argv;
var FEIBCurrencyNameMap = {
    USD: '美金',
    GBP: '英鎊',
    CHF: '瑞士法朗',
    HKD: '港幣',
    AUD: '澳幣',
    SGD: '新加坡幣',
    JPY: '日圓',
    CAD: '加拿大幣',
    CNY: '人民幣',
    ZAR: '南非幣',
    EUR: '歐元',
    NZD: '紐西蘭幣'
};
var FEIBCurrencyTypeEnum;
(function (FEIBCurrencyTypeEnum) {
    FEIBCurrencyTypeEnum["USD"] = "USD";
    FEIBCurrencyTypeEnum["GBP"] = "GBP";
    FEIBCurrencyTypeEnum["CHF"] = "CHF";
    FEIBCurrencyTypeEnum["HKD"] = "HKD";
    FEIBCurrencyTypeEnum["AUD"] = "AUD";
    FEIBCurrencyTypeEnum["SGD"] = "SGD";
    FEIBCurrencyTypeEnum["JPY"] = "JPY";
    FEIBCurrencyTypeEnum["CAD"] = "CAD";
    FEIBCurrencyTypeEnum["CNY"] = "CNY";
    FEIBCurrencyTypeEnum["ZAR"] = "ZAR";
    FEIBCurrencyTypeEnum["EUR"] = "EUR";
    FEIBCurrencyTypeEnum["NZD"] = "NZD";
})(FEIBCurrencyTypeEnum || (FEIBCurrencyTypeEnum = {}));
var FEIBParser = /** @class */ (function (_super) {
    __extends(FEIBParser, _super);
    function FEIBParser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FEIBParser.prototype.getName = function () {
        return '遠東商銀';
    };
    FEIBParser.prototype.resolve = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var page, url, currencyTypes, index, output, _a, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 12, 13, 14]);
                        url = 'https://ebank.feib.com.tw/netbank/servlet/TrxDispatcher?trx=com.lb.wibc.trx.Accessible.AccessibleExchangerate&state=AccessibleExchangerate2&savingType=2';
                        return [4 /*yield*/, engine_1.default.getAvailableContext()];
                    case 1:
                        page = _b.sent();
                        return [4 /*yield*/, page.goto(url)];
                    case 2:
                        _b.sent();
                        log('wait for page to complete ..');
                        return [4 /*yield*/, page.waitFor('#currency > option:nth-child(2)')];
                    case 3:
                        _b.sent();
                        log("parse %s ", url);
                        return [4 /*yield*/, page.evaluate("\n        let result = []\n        document.querySelectorAll('.currency_item select > option').forEach(d => {\n          result.push(d.innerText)\n        })\n        result\n      ")];
                    case 4:
                        currencyTypes = _b.sent();
                        log('supported currency types .. %o', currencyTypes);
                        index = currencyTypes.indexOf(FEIBCurrencyNameMap[options.currencyType]);
                        if (index < 0) {
                            throw "Failed to match currency type " + options.currencyType;
                        }
                        // programmatically set selected index of drop-down menu
                        log('found options at %o .. ', index);
                        return [4 /*yield*/, page.evaluate("\n        document.querySelector('#currency').selectedIndex = " + index + "\n      ")
                            // click the "submit" button
                        ];
                    case 5:
                        _b.sent();
                        // click the "submit" button
                        return [4 /*yield*/, page.click('.currency_item > a > button')];
                    case 6:
                        // click the "submit" button
                        _b.sent();
                        return [4 /*yield*/, page.waitFor('table > tbody > tr:nth-child(2)')];
                    case 7:
                        _b.sent();
                        if (!(options.format === 'csv')) return [3 /*break*/, 9];
                        return [4 /*yield*/, this.toCSV(page)];
                    case 8:
                        _a = _b.sent();
                        return [3 /*break*/, 11];
                    case 9: return [4 /*yield*/, this.toJSON(page)];
                    case 10:
                        _a = _b.sent();
                        _b.label = 11;
                    case 11:
                        output = _a;
                        argv.stdout === 'true' && console.log(output);
                        return [2 /*return*/, {
                                bankCode: 805,
                                currencyCode: options.currencyType,
                                rates: output
                            }];
                    case 12:
                        err_1 = _b.sent();
                        error('error %s', err_1);
                        throw err_1;
                    case 13:
                        page && page.close && page.close();
                        engine_1.default.releaseContext();
                        return [7 /*endfinally*/];
                    case 14: return [2 /*return*/];
                }
            });
        });
    };
    FEIBParser.prototype.toCSV = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, page.evaluate("\n      let csv = ''\n      // parse table data\n      const rows = document.querySelectorAll('tr')\n      for(let i=0; i < rows.length; i++) {\n        for(let j=0; j < rows[i].childElementCount; j++) {\n          csv += rows[i].children[j].textContent + ','\n        }\n        csv += '\n'\n      }\n      // make console return value of \"csv\"\n      csv\n    ")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    FEIBParser.prototype.toJSON = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, page.evaluate("\n      // parse table fields\n      let fields = [\"name\", \"value\"]\n      \n      // parse table data\n      const rows = document.querySelectorAll('tr')\n      let data = []\n      for(let i=1; i < rows.length; i++) {\n        let d = {}\n        for(let j=0; j < rows[i].childElementCount; j++) {\n          const fieldName = fields[j]\n          const value = rows[i].children[j].textContent\n          d[fields[j]] = fieldName === 'value'\n            ? parseFloat(value)\n            : value\n        }\n        data.push(d)\n      }\n      // return value\n      data\n    ")];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return FEIBParser;
}(base_1.BaseParser));
exports.default = FEIBParser;
//# sourceMappingURL=feib.js.map