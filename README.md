# @anue-parser-collection

A project contains parsers using unified protocol

## Prerequisite

- Node.JS > 8

## Installation

Install dependencies

```sh
$ yarn install
```
via `npm`

```sh
$ npm install
```

## Start A Parser

By using the process argument `--use` you can specify which parser you'd like to use.
Additional arguments will be forward to the parser as well.

```sh
$ node build --use=NAME_OF_PARSER \
  # more arguments here
  --arg1=arg1v \
  --arg2=arg2v
  # ...
```

## Batch Execution

Since the headless Chrome has quite a long start time, when you're going to execute 
a bunches of jobs, you can simply passing a `JSON` file to the process.

```sh
$ cat jobs.json | node build/index.js
```

Which will reuse the headless browser and do all the jobs sequentially.

## Debugging

By adding log patterns you can inspect the log during the process

```sh
$ DEBUG=@anue-* node build
```

Also it's possible to make the browser visible by adding `--headless=false`

```sh
$ node build --headless=false
```

## Development

You can debug this project with VSCode built-in debug function

1. Go Debug page
2. select "Start and Debug" then "play".
3. Add breakpoint to any .ts file
